## Potential Webhosters
Common Features	PHP 8, Multiple MySQL databases, Cron Jobs, Email with IMAP

Furthermore	At least 2 "Websites"

Prices disregard any first-year discounts.


|            | Origin | Established | USD/EUR per month | Trouble | Bonus | Looks good to SwordMasterLink |
|------------|--------|-------------|-------------------|---------|-------|------------------------|
| GoDaddy | USA | 1997 | 8,32 | unclear description	 |  |  | 
| tmdhosting.com | USA | 2007 | 12,99 |  | 30 Backups included | |
| ionos.com | Germany | 2000 | 8 |  | No Traffic Limit |  |	
| hostinger.de | Lithuania | 2004 | 7,99 | Max 100 000 visits/month |  | + |
| hostarmada.com | USA | ??? | 16,45 | Somewhat limited Traffic |  |  |
| hostgator.com | USA | 2002 | 5,04 (plus unspecified raises) | Occassionally unpredictable | No Traffic Limit | - |
| bluehost.com | USA | 2003 | 21,99 | unclear description |  | (-) |
| dreamhost.com | USA | 1996 | 8,66 | Wordpress focused |  | (-) |
| hostwinds.com | USA | 2010 | 8,99 |  | No Traffic Limit, IP adress included | + |
| interserver.net | USA | 1999 | 2,5 |  | No Traffic Limit | |
| hetzner.com | Germany | 1997 | 5,39 |  | No Traffic Limit | (-) |
| Scaleway.com | France | 1999 | 9,99 |  |  | (-) |
| ovhcloud.com | France | 1999 | 2,79 (plus unspecified raises) |  | No Traffic Limit | (-) |
